#include <iostream>
#include <mpi.h>
#include <unistd.h>

#include "matrix_class.hpp"

extern "C" {
    void Cblacs_gridinfo(int, int*, int*, int*, int*);
    int Cblacs_pnum(int, int, int);
    /* Cblacs declarations */
    void Cblacs_pinfo(int*, int*);
    void Cblacs_get(int, int, int*);
    void Cblacs_gridinit(int*, const char*, int, int);
    void Cblacs_pcoord(int, int, int*, int*);
    void Cblacs_gridexit(int);
    void Cblacs_barrier(int, const char*);
    void Cdgerv2d(int, int, int, double*, int, int, int);
    void Cdgesd2d(int, int, int, double*, int, int, int);

    int numroc_(int*, int*, int*, int*, int*);
    void pdgemm_(char* TRANSA, char* TRANSB, int* M, int* N, int* K, double* ALPHA, double* A, int* IA, int* JA, int* DESCA, double* B, int* IB, int* JB, int* DESCB, double* BETA, double* C, int* IC, int* JC, int* DESCC);
    void descinit_(int* idescal, int* m, int* n, int* mb, int* nb, int* dummy1, int* dummy2, int* icon, int* procRows, int* info);
    
    int indxg2p_( int *indxglob, int *nb, int *iproc, int *isrcproc, int *nprocs);;
    int indxg2l_( int *indxglob, int *nb, int *iproc, int *isrcproc, int *nprocs);;
}

template <typename T>
Matrix<T> _scatter_matrix(Matrix<T> &A, int A_n_rows, int A_n_cols, int ctxt, int blacs_row, int blacs_col, int blacs_n_rows, int blacs_n_cols, bool mpi_root) {
    /* Helping var */
    int iZERO = 0;
    int blacs_n_procs = blacs_n_cols * blacs_n_rows;
    
    /* calc block size */
    int A_block_n_rows = A_n_rows / blacs_n_rows;
    int A_block_n_cols = A_n_cols / blacs_n_cols;

    /* calc number of rows owned by process (local matrix size) */
    int A_local_n_rows = numroc_(&A_n_rows, &A_block_n_rows, &blacs_row, &iZERO, &blacs_n_rows);
    int A_local_n_cols = numroc_(&A_n_cols, &A_block_n_cols, &blacs_col, &iZERO, &blacs_n_cols);

    for (int id = 0; id < blacs_n_procs; ++id) {
        Cblacs_barrier(ctxt, "All");
    }

    Matrix<T> A_local(A_local_n_rows, A_local_n_cols);

    /* Scatter matrix */
    int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
    for (int r = 0; r < A_n_rows; r += A_block_n_rows, sendr=(sendr+1)%blacs_n_rows) {
        sendc = 0;
        // Number of rows to be sent
        // Is this the last row block?
        int nr = A_block_n_rows;
        if (A_n_rows-r < A_block_n_rows)
            nr = A_n_rows-r;
 
        for (int c = 0; c < A_n_cols; c += A_block_n_cols, sendc=(sendc+1)%blacs_n_cols) {
            // Number of cols to be sent
            // Is this the last col block?
            int nc = A_block_n_cols;
            if (A_n_cols-c < A_block_n_cols)
                nc = A_n_cols-c;
 
            if (mpi_root) {
                // Send a nr-by-nc submatrix to process (sendr, sendc)
                Cdgesd2d(ctxt, nr, nc, A.data() + A_n_rows * c + r, A_n_rows, sendr, sendc);
            }
 
            if (blacs_row == sendr && blacs_col == sendc) {
                // Receive the same data
                // The leading dimension of the local matrix is nrows!
                Cdgerv2d(ctxt, nr, nc, A_local.data()+A_local_n_rows*recvc+recvr, A_local_n_rows, 0, 0);
                recvc = (recvc+nc)%A_local_n_cols;
            }
        }
 
        if (blacs_row == sendr)
            recvr = (recvr+nr)%A_local_n_rows;
    }

    return A_local;
}


template <typename T>
void _gather_matrix(Matrix<T> &A, int A_n_rows, int A_n_cols, Matrix<T> &A_local, int A_local_n_rows, int A_local_n_cols, int ctxt, int blacs_row, int blacs_col, int blacs_n_rows, int blacs_n_cols, bool mpi_root) {
    /* Helping var */
    int iZERO = 0;
    int blacs_n_procs = blacs_n_cols * blacs_n_rows;
    
    /* calc block size */
    int A_block_n_rows = A_n_rows / blacs_n_rows;
    int A_block_n_cols = A_n_cols / blacs_n_cols;

    for (int id = 0; id < blacs_n_procs; ++id) {
        Cblacs_barrier(ctxt, "All");
    }

    /* Gather matrix */
    int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
    for (int r = 0; r < A_n_rows; r += A_block_n_rows, sendr=(sendr+1)%blacs_n_rows) {
        sendc = 0;
        // Number of rows to be sent
        // Is this the last row block?
        int nr = A_block_n_rows;
        if (A_n_rows-r < A_block_n_rows)
            nr = A_n_rows-r;
 
        for (int c = 0; c < A_n_cols; c += A_block_n_cols, sendc=(sendc+1)%blacs_n_cols) {
            // Number of cols to be sent
            // Is this the last col block?
            int nc = A_block_n_cols;
            if (A_n_cols-c < A_block_n_cols)
                nc = A_n_cols-c;
 
            if (blacs_row == sendr && blacs_col == sendc) {
                // Receive the same data
                // The leading dimension of the local matrix is nrows!
                Cdgesd2d(ctxt, nr, nc, A_local.data()+A_local_n_rows*recvc+recvr, A_local_n_rows, 0, 0);
                recvc = (recvc+nc)%A_local_n_cols;
            }

            if (mpi_root) {
                // Send a nr-by-nc submatrix to process (sendr, sendc)
                Cdgerv2d(ctxt, nr, nc, A.data() + A_n_rows * c + r, A_n_rows, sendr, sendc);
            }
        }
 
        if (blacs_row == sendr)
            recvr = (recvr+nr)%A_local_n_rows;
    }
}



template <typename T>
Matrix<T> scatter_matrix(Matrix<T> &A, int A_n_rows, int A_n_cols, int blacs_ctxt, bool mpi_root) {
    /* Helping var */
    int blacs_zero = 0;
    int blacs_n_rows, blacs_n_cols;
    int blacs_row, blacs_col;
    Cblacs_gridinfo(blacs_ctxt, &blacs_n_rows, &blacs_n_cols, &blacs_row, &blacs_col);
    int blacs_n_procs = blacs_n_rows * blacs_n_cols;


    /* calc block size */
    int A_block_n_rows = A_n_rows / blacs_n_rows;
    int A_block_n_cols = A_n_cols / blacs_n_cols;

    /* calc number of rows owned by process (local matrix size) */
    int A_local_n_rows = numroc_(&A_n_rows, &A_block_n_rows, &blacs_row, &blacs_zero, &blacs_n_rows);
    int A_local_n_cols = numroc_(&A_n_cols, &A_block_n_cols, &blacs_col, &blacs_zero, &blacs_n_cols);

    Matrix<T> A_local(A_local_n_rows, A_local_n_cols);

    Cblacs_barrier(blacs_ctxt, "All");

    /* Scatter matrix */
    int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
    for (int r = 0; r < A_n_rows; r += A_block_n_rows, sendr=(sendr+1)%blacs_n_rows) {
        sendc = 0;
        // Number of rows to be sent
        // Is this the last row block?
        int nr = A_block_n_rows;
        if (A_n_rows-r < A_block_n_rows)
            nr = A_n_rows-r;
 
        for (int c = 0; c < A_n_cols; c += A_block_n_cols, sendc=(sendc+1)%blacs_n_cols) {
            // Number of cols to be sent
            // Is this the last col block?
            int nc = A_block_n_cols;
            if (A_n_cols-c < A_block_n_cols)
                nc = A_n_cols-c;
 
            if (mpi_root) {
                // Send a nr-by-nc submatrix to process (sendr, sendc)
                Cdgesd2d(blacs_ctxt, nr, nc, A.data() + A_n_rows * c + r, A_n_rows, sendr, sendc);
            }
 
            if (blacs_row == sendr && blacs_col == sendc) {
                // Receive the same data
                // The leading dimension of the local matrix is nrows!
                Cdgerv2d(blacs_ctxt, nr, nc, A_local.data()+A_local_n_rows*recvc+recvr, A_local_n_rows, 0, 0);
                recvc = (recvc+nc)%A_local_n_cols;
            }
        }
 
        if (blacs_row == sendr)
            recvr = (recvr+nr)%A_local_n_rows;
    }

    return A_local;
}


template <typename T>
Matrix<T>  gather_matrix(Matrix<T> &A_local, int A_n_rows, int A_n_cols, int blacs_ctxt, bool mpi_root) {
    /* Helping var */
    int blacs_zero = 0;
    int blacs_n_rows, blacs_n_cols;
    int blacs_row, blacs_col;
    Cblacs_gridinfo(blacs_ctxt, &blacs_n_rows, &blacs_n_cols, &blacs_row, &blacs_col);
    int blacs_n_procs = blacs_n_rows * blacs_n_cols;
    
    /* calc block size */
    int A_block_n_rows = A_n_rows / blacs_n_rows;
    int A_block_n_cols = A_n_cols / blacs_n_cols;

    int A_local_n_rows = A_local.rows_number();
    int A_local_n_cols = A_local.cols_number();

    Matrix<T> A;
    if (mpi_root) {
        A.resize(A_n_rows, A_n_cols);
    }

    /* Gather matrix */
    int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
    for (int r = 0; r < A_n_rows; r += A_block_n_rows, sendr=(sendr+1)%blacs_n_rows) {
        sendc = 0;
        // Number of rows to be sent
        // Is this the last row block?
        int nr = A_block_n_rows;
        if (A_n_rows-r < A_block_n_rows)
            nr = A_n_rows-r;
 
        for (int c = 0; c < A_n_cols; c += A_block_n_cols, sendc=(sendc+1)%blacs_n_cols) {
            // Number of cols to be sent
            // Is this the last col block?
            int nc = A_block_n_cols;
            if (A_n_cols-c < A_block_n_cols)
                nc = A_n_cols-c;
 
            if (blacs_row == sendr && blacs_col == sendc) {
                // Receive the same data
                // The leading dimension of the local matrix is nrows!
                Cdgesd2d(blacs_ctxt, nr, nc, A_local.data()+A_local_n_rows*recvc+recvr, A_local_n_rows, 0, 0);
                recvc = (recvc+nc)%A_local_n_cols;
            }

            if (mpi_root) {
                // Send a nr-by-nc submatrix to process (sendr, sendc)
                Cdgerv2d(blacs_ctxt, nr, nc, A.data() + A_n_rows * c + r, A_n_rows, sendr, sendc);
            }
        }
 
        if (blacs_row == sendr)
            recvr = (recvr+nr)%A_local_n_rows;
    }

    return A;
}




std::vector<int> get_closest_factors(int n) {
    int d1 = std::sqrt(n);

    while (d1 > 1) { 
        if (n % d1 == 0) {
            break;
        }

        d1 -= 1;
    }

    return std::vector<int> {d1, n / d1};
}


int main(int argc, char** argv) {
    /* MPI init */
    int mpi_rank, mpi_size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    bool mpi_root = (mpi_rank == 0);

    /* BLACS init */
    int blacs_zero = 0, blacs_one = 1, blacs_minus_one = -1;
    int blacs_ctxt, blacs_id, blacs_row, blacs_col, blacs_n_procs;

    auto blacs_grid_shape = get_closest_factors(mpi_size);
    int blacs_n_rows = blacs_grid_shape[0];
    int blacs_n_cols = blacs_grid_shape[1];
    
    Cblacs_pinfo(&blacs_id, &blacs_n_procs);
    Cblacs_get(blacs_minus_one, blacs_zero, &blacs_ctxt);
    Cblacs_gridinit(&blacs_ctxt, "Row-major", blacs_n_rows, blacs_n_cols);
    Cblacs_pcoord(blacs_ctxt, blacs_id, &blacs_row, &blacs_col);

    /* Actual program */
    int dimensions[5];
    Matrix<double> A;
    Matrix<double> B;
    Matrix<double> C_normal_mul;

    if (mpi_root) {
        size_t A_n_rows = 8, A_n_cols = 8;
        size_t B_n_rows = 8, B_n_cols = 8;

        A.resize(A_n_rows, A_n_cols);
        B.resize(B_n_rows, B_n_cols);

        seq_fill_matrix_double(A);
        seq_fill_matrix_double(B);
        
        auto start = MPI_Wtime();
        C_normal_mul = A * B;
        auto end = MPI_Wtime();
        
        std::cout << "Common multiplication time: " << end - start << "s\n";

        int print_matrices = std::max(A.rows_number(), A.cols_number()) < 10 && std::max(B.rows_number(), B.cols_number()) < 10;
        
        if (print_matrices) {
            std::cout << "Matrix A:\n" << A << "\n";
            std::cout << "Matrix B:\n" << B << "\n";
            std::cout << "Matrix C = A * B:\n" << C_normal_mul << "\n";
        }

        dimensions[0] = A.rows_number();
        dimensions[1] = A.cols_number();
        dimensions[2] = B.rows_number();
        dimensions[3] = B.cols_number();
        dimensions[4] = print_matrices;
    }

    /* Send matrix sizes */
    MPI_Bcast(dimensions, 5, MPI_INT, 0, MPI_COMM_WORLD);

    int print_matrices = dimensions[4];
    int A_n_rows = dimensions[0], A_n_cols = dimensions[1];
    int B_n_rows = dimensions[2], B_n_cols = dimensions[3];
    int C_n_rows = A_n_rows, C_n_cols = B_n_cols;

    /* Scatter matrices A and B*/
    auto A_local = scatter_matrix(A, A_n_rows, A_n_cols, blacs_ctxt, mpi_root);
    auto B_local = scatter_matrix(B, B_n_rows, B_n_cols, blacs_ctxt, mpi_root);

    /* print A locals */
    if (print_matrices) {
        for(int id = 0; id < mpi_size; id++) {
            MPI_Barrier(MPI_COMM_WORLD);
            Cblacs_barrier(blacs_ctxt, "All");

            if (mpi_rank == id) {
                std::cout << "A_local on proc " << id << "\n";
                std::cout << A_local << '\n' << std::flush;
            }

            MPI_Barrier(MPI_COMM_WORLD);
            Cblacs_barrier(blacs_ctxt, "All");
        }
    }

    /* print B locals */
    if (print_matrices) {
        for(int id = 0; id < mpi_size; id++) {
            MPI_Barrier(MPI_COMM_WORLD);
            Cblacs_barrier(blacs_ctxt, "All");

            if (mpi_rank == id) {
                std::cout << "B_local on proc " << id << "\n";
                std::cout << B_local << '\n' << std::flush;
            }

            MPI_Barrier(MPI_COMM_WORLD);
            Cblacs_barrier(blacs_ctxt, "All");
        }
    }
    
    /* multiplication */
    int A_block_n_rows = A_n_rows / blacs_n_rows;
    int A_block_n_cols = A_n_cols / blacs_n_cols;
    int B_block_n_rows = B_n_rows / blacs_n_rows;
    int B_block_n_cols = B_n_cols / blacs_n_cols;
    int C_block_n_rows = C_n_rows / blacs_n_rows;
    int C_block_n_cols = C_n_cols / blacs_n_cols;

    auto A_local_n_rows = numroc_(&A_n_rows, &A_block_n_rows, &blacs_row, &blacs_zero, &blacs_n_rows);
    auto A_local_n_cols = numroc_(&A_n_cols, &A_block_n_cols, &blacs_col, &blacs_zero, &blacs_n_cols);

    auto B_local_n_rows = numroc_(&B_n_rows, &B_block_n_rows, &blacs_row, &blacs_zero, &blacs_n_rows);
    auto B_local_n_cols = numroc_(&B_n_cols, &B_block_n_cols, &blacs_col, &blacs_zero, &blacs_n_cols);

    auto C_local_n_rows = numroc_(&C_n_rows, &C_block_n_rows, &blacs_row, &blacs_zero, &blacs_n_rows);
    auto C_local_n_cols = numroc_(&C_n_cols, &C_block_n_cols, &blacs_col, &blacs_zero, &blacs_n_cols);

    int LLD_A = A_local_n_rows;
    int LLD_B = B_local_n_rows;
    int LLD_C = C_local_n_rows;

    int rsrc = 0, csrc = 0, info;
    int* desca = new int[9];
    int* descb = new int[9];
    int* descc = new int[9];

    descinit_(desca, &A_n_rows, &A_n_cols, &A_block_n_rows, &A_block_n_cols, &rsrc, &csrc, &blacs_ctxt, &LLD_A, &info);
    if (info != 0) std::cout << "ERROR OF descinit__A: " << mpi_rank << " " << info << std::endl;
    descinit_(descb, &B_n_rows, &B_n_cols, &B_block_n_rows, &B_block_n_cols, &rsrc, &csrc, &blacs_ctxt, &LLD_B, &info);
    if (info != 0) std::cout << "ERROR OF descinit__B: " << mpi_rank << " " << info << std::endl;
    descinit_(descc, &C_n_rows, &C_n_cols, &C_block_n_rows, &C_block_n_cols, &rsrc, &csrc, &blacs_ctxt, &LLD_C, &info);
    if (info != 0) std::cout << "ERROR OF descinit__C: " << mpi_rank << " " << info << std::endl;

    char N_c = 'N';
    double alpha = 1.0;
    double betta = 0;

    Matrix<double> C_local(C_local_n_rows, C_local_n_cols);
    
    Cblacs_barrier(blacs_ctxt, "All");
    MPI_Barrier(MPI_COMM_WORLD);
    auto start = MPI_Wtime();

    pdgemm_(&N_c, &N_c, &A_n_rows, &B_n_cols, &A_n_cols, &alpha, A_local.data(), &blacs_one, &blacs_one, desca,
            B_local.data(), &blacs_one, &blacs_one, descb,
            &betta, C_local.data(), &blacs_one, &blacs_one, descc);
    
    Cblacs_barrier(blacs_ctxt, "All");
    MPI_Barrier(MPI_COMM_WORLD);
    auto end = MPI_Wtime();
    auto scala_mult_time = end - start;

    /* Print diagonals before gathering */
    if (mpi_root) {
        std::cout << "diagonal elements (distributed): " << std::flush;
    }

    for (int i = 0; i < C_n_rows; i++) {
        MPI_Barrier(MPI_COMM_WORLD);
        Cblacs_barrier(blacs_ctxt, "All");

        int new_i = i + 1;
        int iprow = indxg2p_(&new_i, &C_block_n_rows, &blacs_row, &blacs_zero, &blacs_n_rows);
        int jpcol = indxg2p_(&new_i, &C_block_n_cols, &blacs_col, &blacs_zero, &blacs_n_cols);

        // std::cout << "{" << blacs_row << ", " << blacs_col << "} --- " << "[" << i << ", " << i << "] is in " <<  "[" << iprow << ", " << jpcol << "]\n";

        if (blacs_row == iprow && blacs_col == jpcol) {
            int iloc = indxg2l_(&new_i, &C_block_n_rows, &blacs_row, &blacs_zero, &blacs_n_rows );
            int jloc = indxg2l_(&new_i, &C_block_n_cols, &blacs_col, &blacs_zero, &blacs_n_cols );

            std::cout << C_local(iloc - 1, jloc - 1) << " " << std::flush;
            // std::cout << "[" << C_block_n_rows << ", " << C_block_n_cols << "] " << "(" << i << ", " << i << ") => " << "(" << iloc << "," << jloc << ")" << "of " << "(" << blacs_row << ", " << blacs_col << ")\n" << std::flush;
        }

        MPI_Barrier(MPI_COMM_WORLD);
        Cblacs_barrier(blacs_ctxt, "All");
    }

    MPI_Barrier(MPI_COMM_WORLD);
    Cblacs_barrier(blacs_ctxt, "All");

    if (mpi_root) {
        std::cout << "\n" << std::flush;
    }

    MPI_Barrier(MPI_COMM_WORLD);
    Cblacs_barrier(blacs_ctxt, "All");

    /* print C locals */
    if (print_matrices) {
        for(int id = 0; id < mpi_size; id++) {
            MPI_Barrier(MPI_COMM_WORLD);
            Cblacs_barrier(blacs_ctxt, "All");

            if (mpi_rank == id) {
                std::cout << "C_local on proc " << id << "\n";
                std::cout << C_local << '\n' << std::flush;
            }

            MPI_Barrier(MPI_COMM_WORLD);
            Cblacs_barrier(blacs_ctxt, "All");
        }
    }

    auto C = gather_matrix(C_local, C_n_rows, C_n_cols, blacs_ctxt, mpi_root);

    if (mpi_root) {
        if (print_matrices) {
            std::cout << "Matrix C = A * B:\n" << C << '\n';
        }

        std::cout << "Normal multiplication diagonal: ";
        C_normal_mul.print_diagonal();
        std::cout << "scalap multiplication diagonal: ";
        C.print_diagonal();

        std::cout << "SCALA mult time: " << scala_mult_time << "s\n";
        std::cout << "C == CN: " << compare_matrices_double(C, C_normal_mul) << "\n";
    }

    MPI_Finalize();
    return 0;
}
