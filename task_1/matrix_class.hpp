#include <vector>
#include <fstream>
#include <random>

template <typename T>
class Matrix {
public:
    Matrix(size_t n_rows=0, size_t n_cols=0) : n_rows_(n_rows), n_cols_(n_cols) { 
        matrix_.resize(n_rows_ * n_cols_); 
    }

    size_t rows_number() const {return n_rows_; }
    size_t cols_number() const {return n_cols_; }
    T* data() { return matrix_.data(); }
    const T* data() const { return matrix_.data(); }

    void resize(size_t n_rows, size_t n_cols);
    void transpose_inplace();
    void transpose_cursed();
    void print_diagonal() const;
    void print_cursed(std::ostream& os) const;

    T& operator()(size_t row, size_t col) { return matrix_[col * n_rows_ + row]; }
    const T& operator()(size_t row, size_t col) const { return matrix_[col * n_rows_ + row]; }

    friend std::ostream& operator<<(std::ostream& os, const Matrix<T>& matrix) {
        auto rows_n = matrix.rows_number();
        auto cols_n = matrix.cols_number();

        for (size_t i = 0; i < rows_n; i++) {
            for (size_t j = 0; j < cols_n; j++) {
                os << std::setw(5) << std::setprecision(5) << matrix(i, j) << ' ';
            }
            os << '\n';
        }
        return os;
    }

    Matrix<T> operator*(const Matrix<T>& other) {
        Matrix<T> result(n_rows_, other.n_cols_);

        for (size_t i = 0; i < n_rows_; ++i) {
            for (size_t j = 0; j < other.n_cols_; ++j) {
                T sum = T();
                for (size_t k = 0; k < n_cols_; ++k) {
                    sum += (*this)(i, k) * other(k, j);
                }
                result(i, j) = sum;
            }
        }

        return result;
    }

    bool save_to_file(const std::string& filename);
    bool load_from_file(const std::string& filename);

private:
    size_t n_rows_;
    size_t n_cols_;
    std::vector<T> matrix_;
};

void random_fill_matrix_double(Matrix<double>& matrix, double lower_bound, double upper_bound);
bool compare_matrices_double(Matrix<double> A, Matrix<double> B, double eps=1e-6);


template <typename T>
void Matrix<T>::resize(size_t n_rows, size_t n_cols) {
    n_rows_ = n_rows;
    n_cols_ = n_cols;
    matrix_.resize(n_rows_ * n_cols_);
}

template <typename T>
void Matrix<T>::transpose_inplace() {
    std::vector<T> matrix_t(n_rows_ * n_cols_);

    for (size_t i = 0; i < n_rows_; ++i) {
        for (size_t j = 0; j < n_cols_; ++j) {
            matrix_t[j * n_rows_ + i] = matrix_[i * n_cols_ + j];
        }
    }

    auto tmp = n_rows_;
    n_rows_ = n_cols_;
    n_cols_ = tmp;

    matrix_ = matrix_t;
}

template <typename T>
void Matrix<T>::transpose_cursed() {
    auto tmp = n_rows_;
    n_rows_ = n_cols_;
    n_cols_ = tmp;
}

template <typename T>
void Matrix<T>::print_diagonal() const {
    auto diag_size = std::min(n_rows_, n_cols_);
    for (int i = 0; i < diag_size; i++) {
        std::cout << matrix_[i * n_rows_ + i] << " ";
    }
    std::cout << "\n";
}

template <typename T>
void Matrix<T>::print_cursed(std::ostream& os) const{
        auto raw_data = data();

        for (size_t i = 0; i < n_cols_; i++) {
            for (size_t j = 0; j < n_rows_; j++) {
                os << std::setw(5) << std::setprecision(2) << raw_data[n_rows_ * i + j] << ' ';
            }
            os << '\n';
        }
}

template <typename T>
bool Matrix<T>::load_from_file(const std::string& filename) {
    std::ifstream file(filename, std::ios::in | std::ios::binary);
    if (!file.is_open()) {
        return false;
    }

    size_t loaded_rows, loaded_cols;
    file.read(reinterpret_cast<char*>(&loaded_rows), sizeof(loaded_rows));
    file.read(reinterpret_cast<char*>(&loaded_cols), sizeof(loaded_cols));

    if (loaded_rows != n_rows_ || loaded_cols != n_cols_) {
        n_rows_ = loaded_rows;
        n_cols_ = loaded_cols;

        matrix_.resize(n_rows_ * n_cols_);
    }

    file.read(reinterpret_cast<char*>(matrix_.data()), sizeof(T) * n_rows_ * n_cols_);
    file.close();
    return true;
}

template <typename T>
bool Matrix<T>::save_to_file(const std::string& filename) {
        std::ofstream file(filename, std::ios::out | std::ios::binary);
        if (!file.is_open()) {
            return false;
        }

        file.write(reinterpret_cast<char*>(&n_rows_), sizeof(n_rows_));
        file.write(reinterpret_cast<char*>(&n_cols_), sizeof(n_cols_));
        file.write(reinterpret_cast<char*>(matrix_.data()), sizeof(T) * n_rows_ * n_cols_);
        file.close();
        return true;
}

void random_fill_matrix_double(Matrix<double>& matrix, double lower_bound, double upper_bound) {
    std::random_device rd;
    std::mt19937 gen(rd());
    
    std::uniform_real_distribution<double> distribution(lower_bound, upper_bound);

    size_t n_rows = matrix.rows_number();
    size_t n_cols = matrix.cols_number();

    for(int i = 0; i < n_rows; i++) {
        for(int j = 0; j < n_cols; j++) {
            matrix(i, j) = distribution(gen);
        }
    }
}


void seq_fill_matrix_double(Matrix<double>& matrix) {
    size_t n_rows = matrix.rows_number();
    size_t n_cols = matrix.cols_number();

    for(int i = 0; i < n_rows; i++) {
        for(int j = 0; j < n_cols; j++) {
            matrix(i, j) = i * n_cols + j;
        }
    }
}


bool compare_matrices_double(Matrix<double> A, Matrix<double> B, double eps) {
    auto rows_number = A.rows_number();
    auto cols_number = A.cols_number();

    if (rows_number != B.rows_number() || cols_number != B.cols_number()) {
        return false;
    }

    for(int i = 0; i < rows_number; i++) {
        for(int j = 0; j < cols_number; j++) {
            if (std::abs(A(i, j) - B(i, j)) > eps) {
                return false;
            }
        }
    }

    return true;
}
