#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <string>
#include <mpi.h>
#include <iomanip>


extern "C" {
    void Cblacs_gridinfo(int, int*, int*, int*, int*);
    int Cblacs_pnum(int, int, int);
    /* Cblacs declarations */
    void Cblacs_pinfo(int*, int*);
    void Cblacs_get(int, int, int*);
    void Cblacs_gridinit(int*, const char*, int, int);
    void Cblacs_pcoord(int, int, int*, int*);
    void Cblacs_gridexit(int);
    void Cblacs_barrier(int, const char*);
    void Cdgerv2d(int, int, int, double*, int, int, int);
    void Cdgesd2d(int, int, int, double*, int, int, int);

    int numroc_(int*, int*, int*, int*, int*);
    void pdgemm_(char* TRANSA,
                 char* TRANSB,
                 int* M,
                 int* N,
                 int* K,
                 double* ALPHA,
                 double* A,
                 int* IA,
                 int* JA,
                 int* DESCA,
                 double* B,
                 int* IB,
                 int* JB,
                 int* DESCB,
                 double* BETA,
                 double* C,
                 int* IC,
                 int* JC,
                 int* DESCC);
    void descinit_(int* idescal,
                   int* m,
                   int* n,
                   int* mb,
                   int* nb,
                   int* dummy1,
                   int* dummy2,
                   int* icon,
                   int* procRows,
                   int* info);
}



void random_fill_matrix_double(Matrix<double>& matrix, double lower_bound, double upper_bound) {
    std::random_device rd;
    std::mt19937 gen(rd());
    
    std::uniform_real_distribution<double> distribution(lower_bound, upper_bound);

    size_t n_rows = matrix.rows_number();
    size_t n_cols = matrix.cols_number();

    for(int i = 0; i < n_rows; i++) {
        for(int j = 0; j < n_cols; j++) {
            matrix(i, j) = distribution(gen);
        }
    }
}

std::vector<int> get_closest_factors(int n) {
    int d1 = std::sqrt(n);

    while (d1 > 1) { 
        if (n % d1 == 0) {
            break;
        }

        d1 -= 1;
    }

    return std::vector<int> {d1, n / d1};
}

// Рассылка блоков
template <typename T>
Matrix<T> scatter_matrix(Matrix<T> matrix, int root_id) {
    int iZERO = 0;

    int matrix_rows_number;
    int matrix_cols_number;

    Matrix<T> local_matrix(0, 0);

    // get mpi info
    int mpi_rank, mpi_size;
    
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    bool mpi_root = (mpi_rank == root_id);

    // get blacs info
    int blacs_proc_number, blacs_id;
    Cblacs_pinfo(&blacs_id, &blacs_proc_number);

    int ctxt, blacs_row, blacs_col;
    Cblacs_get(0, 0, &ctxt);
    Cblacs_pcoord(ctxt, blacs_id, &blacs_row, &blacs_col);
    
    std::vector<int> blacs_grid_size = get_closest_factors(blacs_proc_number);

    int blacs_rows_number = blacs_grid_size[0];
    int blacs_cols_number = blacs_grid_size[1];

    // send matrix and grid info
    int dimensions[4];
    if (mpi_root) {
        dimensions[0] = matrix.rows_number();
        dimensions[1] = matrix.cols_number();
        dimensions[2] = matrix.rows_number() / blacs_rows_number;
        dimensions[3] = matrix.cols_number() / blacs_cols_number;

        // std::cout << "\n";
        // std::cout << "matrix_rows_number: " << dimensions[0] << "\n";
        // std::cout << "matrix_cols_number: " << dimensions[1] << "\n";
        // std::cout << "block_rows_number: " << dimensions[2] << "\n";
        // std::cout << "block_cols_number: " << dimensions[3] << "\n";
        // std::cout << "\n";
    }

    MPI_Bcast(dimensions, 4, MPI_INT, 0, MPI_COMM_WORLD);

    matrix_rows_number = dimensions[0];
    matrix_cols_number = dimensions[1];
    int block_rows_number = dimensions[2];
    int block_cols_number = dimensions[3];

    // number of rows owned by current process
    int nrows = numroc_(&matrix_rows_number, &block_rows_number, &blacs_row, &iZERO, &blacs_rows_number);
    int ncols = numroc_(&matrix_cols_number, &block_cols_number, &blacs_col, &iZERO, &blacs_cols_number);

    // std::cout << "Process " + std::to_string(blacs_row) + "," + std::to_string(blacs_col) + ": " + std::to_string(nrows) + "," + std::to_string(ncols) + "\n";

    for (int id = 0; id < blacs_proc_number; ++id) {
        Cblacs_barrier(ctxt, "All");
    }

    // /* Scatter matrix */
    // int send_row = 0, send_col = 0, recv_row = 0, recv_col = 0;
    // for (int row = 0; row < matrix_rows_number; row += block_rows_number, send_row=(send_row+1) % blacs_rows_number) {
    //     send_col = 0;
    //     // Number of rows to be sent
    //     // Is this the last row block?
    //     int send_rows_num = block_rows_number;
    //     if (matrix_rows_number - row < block_rows_number) {
    //         send_rows_num = matrix_rows_number - row;
    //     }
    
    //     for (int col = 0; col < matrix_cols_number; col += block_cols_number, send_col=(send_col+1)%blacs_cols_number) {
    //         // Number of cols to be sent
    //         // Is this the last col block?
    //         int send_cols_num = block_cols_number;
    //         if (matrix_cols_number - col < block_cols_number)
    //             send_cols_num = matrix_cols_number - col;
    
    //         if (mpi_root) {
    //             // Send a nr-by-nc submatrix to process (sendr, sendc)
    //             Cdgesd2d(ctxt, send_rows_num, send_cols_num, matrix.data() + matrix_rows_number * col + row, matrix_rows_number, send_row, send_col);
    //         }
    
    //         if (blacs_row == send_row && blacs_col == send_col) {
    //             // Receive the same data
    //             // The leading dimension of the local matrix is nrows!
    //             std::cout << "Proc " + std::to_string(blacs_row) + "," + std::to_string(blacs_col) + " sends: " + std::to_string(send_rows_num) + "," + std::to_string(send_cols_num) + "\n";
    //             local_matrix.resize(send_rows_num, send_cols_num);
    //             Cdgerv2d(ctxt, send_rows_num, send_cols_num, local_matrix.data()+nrows*recv_col+recv_row, nrows, 0, 0);
    //             recv_col = (recv_col+send_cols_num)%ncols;
    //         }
    
    //     }
    
    //     if (row == send_row)
    //         recv_row = (recv_row + send_rows_num) % nrows;
    // }

    /* Scatter matrix */
int N = matrix_rows_number, Nb = block_rows_number;
int M = matrix_cols_number, Mb = block_cols_number;
int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
for (int r = 0; r < N; r += Nb, sendr=(sendr+1)%blacs_rows_number) {
    sendc = 0;
    // Number of rows to be sent
    // Is this the last row block?
    int nr = Nb;
    if (N-r < Nb)
        nr = N-r;
 
    for (int c = 0; c < M; c += Mb, sendc=(sendc+1)%blacs_cols_number) {
        // Number of cols to be sent
        // Is this the last col block?
        int nc = Mb;
        if (M-c < Mb)
            nc = M-c;
 
        if (mpi_root) {
            // Send a nr-by-nc submatrix to process (sendr, sendc)
            Cdgesd2d(ctxt, nr, nc, matrix.data()+N*c+r, N, sendr, sendc);
        }
 
        if (blacs_row == sendr && blacs_col == sendc) {
            // Receive the same data
            // The leading dimension of the local matrix is nrows!
            local_matrix.resize(nr, nc);
            Cdgerv2d(ctxt, nr, nc, local_matrix.data()+nrows*recvc+recvr, nrows, 0, 0);
            recvc = (recvc+nc)%ncols;
        }
 
    }
 
    if (blacs_row == sendr)
        recvr = (recvr+nr)%nrows;
}
    
    return local_matrix;
}

// сборка блоков
template <typename T>
void gather_matrix(Matrix<T> &A, Matrix<T> &A_local, int root_id) {
    int iZERO = 0;

    int matrix_rows_number;
    int matrix_cols_number;

    Matrix<T> local_matrix(0, 0);

    // get mpi info
    int mpi_rank, mpi_size;
    
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    bool mpi_root = (mpi_rank == root_id);

    // get blacs info
    int blacs_proc_number, blacs_id;
    Cblacs_pinfo(&blacs_id, &blacs_proc_number);

    int ctxt, blacs_row, blacs_col;
    Cblacs_get(0, 0, &ctxt);
    Cblacs_pcoord(ctxt, blacs_id, &blacs_row, &blacs_col);
    
    std::vector<int> blacs_grid_size = get_closest_factors(blacs_proc_number);

    int blacs_rows_number = blacs_grid_size[0];
    int blacs_cols_number = blacs_grid_size[1];

    // send matrix and grid info
    int dimensions[4];
    if (mpi_root) {
        dimensions[0] = A.rows_number();
        dimensions[1] = A.cols_number();
        dimensions[2] = A.rows_number() / blacs_rows_number;
        dimensions[3] = A.cols_number() / blacs_cols_number;
    }

    MPI_Bcast(dimensions, 4, MPI_INT, 0, MPI_COMM_WORLD);

    matrix_rows_number = dimensions[0];
    matrix_cols_number = dimensions[1];
    int block_rows_number = dimensions[2];
    int block_cols_number = dimensions[3];

    // number of rows owned by current process
    int nrows = numroc_(&matrix_rows_number, &block_rows_number, &blacs_row, &iZERO, &blacs_rows_number);
    int ncols = numroc_(&matrix_cols_number, &block_cols_number, &blacs_col, &iZERO, &blacs_cols_number);

    for (int id = 0; id < blacs_proc_number; ++id) {
        Cblacs_barrier(ctxt, "All");
    }

    int send_row = 0, send_col = 0, recv_row = 0, recv_col = 0;
    for (int row = 0; row < matrix_rows_number; row += block_rows_number, send_row=(send_row+1)%blacs_rows_number) {
        send_col = 0;
        // Number of rows to be sent
        // Is this the last row block?
        int send_rows_number = block_rows_number;
        if (matrix_rows_number-row < blacs_rows_number)
            send_rows_number = matrix_rows_number-row;
 
        for (int col = 0; col < matrix_cols_number; col += block_cols_number, send_col=(send_col+1)%blacs_cols_number) {
            // Number of cols to be sent
            // Is this the last col block?
            int send_cols_number = block_cols_number;
            if (matrix_cols_number-col < block_cols_number)
                send_cols_number = matrix_cols_number-col;
 
            if (blacs_row == send_row && blacs_col == send_col) {
                // Send a nr-by-nc submatrix to process (sendr, sendc)
                // A_local.resize(send_rows_number, send_cols_number);
                Cdgesd2d(ctxt, send_rows_number, send_cols_number, A_local.data()+nrows*recv_col+recv_row, nrows, 0, 0);
                recv_col = (recv_col+send_cols_number)%ncols;
            }
 
            if (mpi_root) {
                // Receive the same data
                // The leading dimension of the local matrix is nrows!
                Cdgerv2d(ctxt, send_rows_number, send_cols_number, A.data()+matrix_rows_number*col+row, matrix_rows_number, send_row, send_col);
            }
 
        }
 
        if (blacs_row == send_row)
            recv_row = (recv_row+send_rows_number)%nrows;
    }
}

bool compare_matrices_double(Matrix<double> A, Matrix<double> B, double eps=1e-6) {
    auto rows_number = A.rows_number();
    auto cols_number = A.cols_number();

    if (rows_number != B.rows_number() || cols_number != B.cols_number()) {
        return false;
    }

    for(int i = 0; i < rows_number; i++) {
        for(int j = 0; j < cols_number; j++) {
            if (std::abs(A(i, j) - B(i, j)) > eps) {
                return false;
            }
        }
    }

    return true;
}

int main(int argc, char** argv) {
    int mpi_root_id = 0;
    int iZERO = 0;
    int iONE = 1;
    int iMINUS_ONE = -1;

    Matrix<double> A(8, 8);
    Matrix<double> B(8, 8);
    Matrix<double> C(A.rows_number(), B.cols_number());
    auto C_normal = A * B;


    int mpi_rank, mpi_size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    bool mpi_root = (mpi_rank == 0);
    
    /* Helping vars */
    if (mpi_root) {
        random_fill_matrix_double(B, 0, 1);
        random_fill_matrix_double(A, 0, 1);

        std::cout << "Matrix A in root:\n";
        std::cout << A;
        std::cout << "Matrix B in root:\n";
        std::cout << B;
        
        C_normal = A * B;
        std::cout << "Matrix C = A * B in root:\n";
        std::cout << C_normal << "\n";
        C_normal.save_to_file("normal_mul.mat");
    }

    MPI_Barrier(MPI_COMM_WORLD);

    /* BLACS Init*/
    int blacs_proc_number, blacs_id;
    Cblacs_pinfo(&blacs_id, &blacs_proc_number);

    std::vector<int> blacs_grid_size = get_closest_factors(blacs_proc_number);

    int blacs_rows_number = blacs_grid_size[0];
    int blacs_cols_number = blacs_grid_size[1];

    int ctxt, blacs_row, blacs_col;

    Cblacs_get(iMINUS_ONE, iZERO, &ctxt);
    Cblacs_gridinit(&ctxt, "Row-major", blacs_rows_number, blacs_cols_number);
    Cblacs_pcoord(ctxt, blacs_id, &blacs_row, &blacs_col);

    std::cout << std::to_string(blacs_id) + ": " + std::to_string(blacs_row) + ", " + std::to_string(blacs_col) + "\n";


    MPI_Barrier(MPI_COMM_WORLD);
    if (mpi_root) {
        A.transpose_inplace();
        B.transpose_inplace();

        std::cout << "Matrix A^T in root: " << A.rows_number() << "x" << A.cols_number() << ":\n";
        std::cout << A;
        std::cout << "Matrix B^T in root: " << B.rows_number() << "x" << B.cols_number() << ":\n";
        std::cout << B;
    }

    // auto A_local = scatter_matrix(A, mpi_root_id);
    Matrix<double> A_local(0, 0);

    /* SHIT */



int N = A.rows_number(), Nb = N / blacs_rows_number;
int M =  A.cols_number(), Mb = M / blacs_cols_number;

    int nrows = numroc_(&N, &Nb, &blacs_row, &iZERO, &blacs_rows_number);
    int ncols = numroc_(&M, &Mb, &blacs_col, &iZERO, &blacs_cols_number);

int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
for (int r = 0; r < N; r += Nb, sendr=(sendr+1)%blacs_rows_number) {
    sendc = 0;
    // Number of rows to be sent
    // Is this the last row block?
    int nr = Nb;
    if (N-r < Nb)
        nr = N-r;
 
    for (int c = 0; c < M; c += Mb, sendc=(sendc+1)%blacs_cols_number) {
        // Number of cols to be sent
        // Is this the last col block?
        int nc = Mb;
        if (M-c < Mb)
            nc = M-c;
 
        if (mpi_root) {
            // Send a nr-by-nc submatrix to process (sendr, sendc)
            Cdgesd2d(ctxt, nr, nc, A.data()+N*c+r, N, sendr, sendc);
        }
 
        if (blacs_row == sendr && blacs_col == sendc) {
            // Receive the same data
            // The leading dimension of the local matrix is nrows!
            A_local.resize(nr, nc);
            Cdgerv2d(ctxt, nr, nc, A_local.data()+nrows*recvc+recvr, nrows, 0, 0);
            recvc = (recvc+nc)%ncols;
        }
 
    }
 
    if (blacs_row == sendr)
        recvr = (recvr+nr)%nrows;
}

    /* SHIT END */

    auto B_local = scatter_matrix(B, mpi_root_id);

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);

    std::flush(std::cout);

    /* Print A Locals */
    for (int id = 0; id < mpi_size; ++id) {
        if (id == blacs_id) {
            std::cout << "A on proc " << id << std::endl;
            std::cout << A_local;
            std::flush(std::cout);
            std::cout << std::endl;
        }

        MPI_Barrier(MPI_COMM_WORLD);
    }

    /* Print B Locals */
    for (int id = 0; id < mpi_size; ++id) {
        if (id == mpi_rank) {
            std::cout << "B on proc " << id << "\n";
            std::cout << B_local;
            std::flush(std::cout);
            std::cout << "\n";
        }

        MPI_Barrier(MPI_COMM_WORLD);
    }

    /* C locals */
    int A_rows_number = A.rows_number(), A_cols_number = A.cols_number();
    int B_rows_number = B.rows_number(), B_cols_number = B.cols_number();

    int A_block_rows = A_rows_number / blacs_rows_number;
    int A_block_cols = A_cols_number / blacs_cols_number;
    int B_block_rows = B_rows_number / blacs_rows_number;
    int B_block_cols = B_cols_number / blacs_cols_number;

    auto nrows_C = numroc_(&A_rows_number, &A_block_rows, &blacs_row, &iZERO, &blacs_rows_number);
    auto ncols_C = numroc_(&B_cols_number, &B_block_cols, &blacs_col, &iZERO, &blacs_cols_number);

    int nrows_A = numroc_(&A_rows_number, &A_block_rows, &blacs_row, &iZERO, &blacs_rows_number);
    int ncols_A = numroc_(&A_cols_number, &A_block_cols, &blacs_col, &iZERO, &blacs_cols_number);

    int nrows_B = numroc_(&B_rows_number, &B_block_rows, &blacs_row, &iZERO, &blacs_rows_number);
    int ncols_B = numroc_(&B_cols_number, &B_block_cols, &blacs_col, &iZERO, &blacs_cols_number);

    Matrix<double> C_local(nrows_C, ncols_C);

    int LLD_A = nrows_A;
    int LLD_B = nrows_B;
    int LLD_C = nrows_C;

    int rsrc = 0, csrc = 0, info;
    int* desca = new int[9];
    int* descb = new int[9];
    int* descc = new int[9];

    descinit_(desca, &A_rows_number, &A_cols_number, &A_block_rows, &A_block_cols, &rsrc, &csrc, &ctxt, &LLD_A, &info);
    if (info != 0) std::cout << "ERROR OF descinit__A: " << mpi_rank << " " << info << std::endl;
    descinit_(descb, &B_rows_number, &B_cols_number, &B_block_rows, &B_block_cols, &rsrc, &csrc, &ctxt, &LLD_B, &info);
    if (info != 0) std::cout << "ERROR OF descinit__B: " << mpi_rank << " " << info << std::endl;
    descinit_(descc, &A_rows_number, &B_cols_number, &A_block_rows, &B_block_cols, &rsrc, &csrc, &ctxt, &LLD_C, &info);
    if (info != 0) std::cout << "ERROR OF descinit__C: " << mpi_rank << " " << info << std::endl;

    char N_c = 'N';
    double alpha = 1.0;
    double betta = 0;
    
    pdgemm_(&N_c, &N_c, &A_rows_number, &B_cols_number, &A_cols_number, &alpha, A_local.data(), &iONE, &iONE, desca,
            B_local.data(), &iONE, &iONE, descb,
            &betta, C_local.data(), &iONE, &iONE, descc);
    //pdgemv_(&N, &n_a, &m_a, &alpha, localA.data(), &iONE, &iONE, desca,
    //                                localX.data(), &iONE, &iONE, descb, &iONE,
    //                                &betta, localC.data(), &iONE, &iONE, descc, &iONE);


    /* Print С Locals */
    for (int id = 0; id < mpi_size; ++id) {
        if (id == mpi_rank) {
            std::cout << "C on proc " << id << "\n";
            std::cout << C_local;
            std::flush(std::cout);
            std::cout << "\n";
        }

        MPI_Barrier(MPI_COMM_WORLD);
    }

    if (mpi_root) {
        C.transpose_inplace();
        std::cout << "C size before gather: " << C.rows_number() << "x" << C.cols_number() << "\n";
    }

    gather_matrix(C, C_local, mpi_root_id);

    if (mpi_root) {
        C.transpose_inplace();
        std::cout << "C size after gather: " << C.rows_number() << "x" << C.cols_number() << "\n";
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if (mpi_root) {
        std::cout << "SCALA C: \n" << C << "\n";
        C.save_to_file("scala_mul.mat");

        bool same = compare_matrices_double(C, C_normal);

        std::cout << "Matrices same: " << same << "\n";
    }

    Cblacs_gridexit(ctxt);
    MPI_Finalize();

    return 0;
}
